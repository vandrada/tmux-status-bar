pub const BLACK: &str = "black";
pub const RED: &str = "red";
pub const GREEN: &str = "green";
pub const YELLOW: &str = "yellow";
pub const BLUE: &str = "blue";
pub const MAGENTA: &str = "magenta";
pub const CYAN: &str = "cyan";
pub const WHITE: &str = "white";
pub const BRIGHTBLACK: &str = "brightblack";
pub const BRIGHTRED: &str = "brightred";
pub const BRIGHTGREEN: &str = "brightgreen";
pub const BRIGHTYELLOW: &str = "brightyellow";
pub const BRIGHTBLUE: &str = "brightblue";
pub const BRIGHTMAGENTA: &str = "brightmagenta";
pub const BRIGHTCYAN: &str = "brightcyan";
pub const BRIGHTWHITE: &str = "brightwhite";

#[derive(Debug, PartialEq)]
pub struct Background<'a>(pub &'a str);

#[derive(Debug, PartialEq)]
pub struct Foreground<'a>(pub &'a str);
