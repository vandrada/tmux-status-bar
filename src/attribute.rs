use std::iter::Iterator;

/// Tmux attributes that can be applied to a segment
#[allow(dead_code)]
#[derive(Debug)]
pub enum Attribute {
    Bold,
    Dim,
    Underscore,
    Blink,
    Reverse,
    Hidden,
    Italics,
    Strikethrough,
    NoBold,
    NoDim,
    NoUnderscore,
    NoBlink,
    NoReverse,
    NoHidden,
    NoItalics,
    NoStrikethrough,
}

impl Attribute {
    fn to_string(&self) -> &'static str {
        match *self {
            Attribute::Bold => "bold",
            Attribute::Dim => "dim",
            Attribute::Underscore => "underscore",
            Attribute::Blink => "blink",
            Attribute::Reverse => "reverse",
            Attribute::Hidden => "hidden",
            Attribute::Italics => "italics",
            Attribute::Strikethrough => "strikethrough",
            Attribute::NoBold => "nobold",
            Attribute::NoDim => "nodim",
            Attribute::NoUnderscore => "nounderscore",
            Attribute::NoBlink => "noblink",
            Attribute::NoReverse => "noreverse",
            Attribute::NoHidden => "nohidden",
            Attribute::NoItalics => "noitalics",
            Attribute::NoStrikethrough => "nostrikethrough",
        }
    }

    /// Creates the attribute string to be applied to a segment
    ///
    /// # Arguments
    /// * `attributes` - a `Vec` of attributes
    pub fn attribute_string(attributes: &[Attribute]) -> String {
        attributes
            .iter()
            .map(|att| att.to_string())
            .collect::<Vec<_>>()
            .join(",")
    }
}
