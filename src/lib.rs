/// # Tmux
/// Simple module to print strings formatted for tmux.
///
/// Options include:
/// * setting the foreground color
/// * setting the background color
/// * setting any attributes
/// * setting the string itself.
///
/// Comes with two APIs, a struct-based one and a single module function
/// `tmuxify`.
///
/// Examples:
///
/// ## Tmux Struct
/// ```rust
/// use tmux::Tmux;
/// use tmux::color::*;
/// use tmux::attribute::*;
///
/// let mut tmux = Tmux::new();
/// tmux.set_foreground(Foreground(23));
/// tmux.set_background(Background(233));
/// tmux.set_attributes(Some(vec![Attribute::Bold]));
/// tmux.set_message("Hello world".into());
/// tmux.show();
/// ```
///
/// ## Tmuxify
/// ```rust
/// use tmux::*;
/// use tmux::color::*;
/// use tmux::attribute::*;
///
/// let tmux_string = Tmux::tmuxify(
///     Foreground(23),
///     Background(233),
///     Some(vec![Attribute::Bold]),
///     "Hello World".into(),
/// );
/// println!("{}", tmux_string);
/// ```

pub mod color;
use color::*;

pub mod attribute;
use attribute::*;

#[derive(Debug)]
pub struct Tmux<'a> {
    /// The segment's foreground color
    fg_color: Foreground<'a>,
    /// The segment's background color
    bg_color: Background<'a>,
    /// What string to print
    message: String,
    /// A list of optional attributes to apply
    attributes: Option<Vec<Attribute>>,
}

impl<'a> Default for Tmux<'a> {
    fn default() -> Self {
        Self::new()
    }
}

fn work(
    fg: &Foreground,
    bg: &Background,
    attributes: &Option<Vec<Attribute>>,
    message: &str,
) -> String {
    let Foreground(fg_color) = *fg;
    let Background(bg_color) = *bg;

    let mut att_string = String::new();
    if let Some(ref atts) = *attributes {
        att_string.push_str(",");
        att_string.push_str(&Attribute::attribute_string(atts));
    }

    format!(
        "#[fg={},bg={}{}]{}",
        fg_color,
        bg_color,
        att_string,
        message
    )
}

#[allow(dead_code)]
impl<'a> Tmux<'a> {
    /// Module function to create a Tmux segment
    ///
    /// # Arguments
    /// * `fg` - The foreground color to use
    /// * `bg` - The background color to use
    /// * `attributes` - A vec of `Attributes` to apply
    /// * `message` - The string to print
    pub fn tmuxify(
        fg: Foreground,
        bg: Background,
        attributes: Option<Vec<Attribute>>,
        message: &str,
    ) -> String {
        work(&fg, &bg, &attributes, message)
    }

    /// Creates a new `Tmux` struct
    pub fn new() -> Tmux<'a> {
        Tmux {
            fg_color: Foreground("default"),
            bg_color: Background("default"),
            message: String::new(),
            attributes: None,
        }
    }

    /// Sets the message to be printed
    pub fn set_message(&mut self, message: &str) {
        self.message = String::from(message);
    }

    /// Sets the background color
    pub fn set_background(&mut self, background: Background<'a>) {
        self.bg_color = background;
    }

    /// Sets the foreground color
    pub fn set_foreground(&mut self, foreground: Foreground<'a>) {
        self.fg_color = foreground;
    }

    /// Sets the attributes
    pub fn set_attributes(&mut self, attributes: Option<Vec<Attribute>>) {
        self.attributes = attributes
    }

    /// Formats and returns the Tmux segment
    pub fn show(&self) -> String {
        work(
            &self.fg_color,
            &self.bg_color,
            &self.attributes,
            &self.message,
        )
    }
}
